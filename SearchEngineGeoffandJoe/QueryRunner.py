
import requests
from elasticsearch import Elasticsearch

es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
r = requests.get('http://localhost:9200')
id="ebuyer"
dt1 = "Pages"


def Output(e):

    a=0
    b = 0
    results = []
    if(a==0):
        if (b == 0):
            for y in e['hits']['hits']:
                try:
                    results.append(int(y['_id']))
                except:
                    pass

            b=b+1
    a=a+1

    return results


def ORSearch(Esearch, index, variable, fuz, slop, size):

    e = Esearch.search(index=index, doc_type=dt1, body={
        "query": {
            "multi_match": {
                "fields": ["_all"],
                "query": variable,
                "fuzziness": fuz,
                "operator": "or",
                "slop": slop

            }
        }
        , "size": size
    })

    return e

def ANDSearch(Esearch, index, variable,fuz,slop, size):
    e=Esearch.search(index=index,doc_type=dt1, body={
        "query": {
            "multi_match": {
                "fields": ["_all"],
                "query": variable,
                "fuzziness": fuz,
                "operator": "and",
                "slop": slop

            }
        }
        , "size": size
    })

    return e


def andORandSearch(Esearch, index, variable1, variable2, fuz, size):
   z = Esearch.search(index=index,doc_type=dt1, body={
        "query": {
            "bool": {
                "should": [
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable1,
                            "fuzziness": fuz,
                            "operator": "and"
                        }
                    },
                    # OR
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable2,
                            "fuzziness": fuz,
                            "operator": "and"
                        }
                    }

                ]
            }
        }
       , "size": size
    })

   return z


def OrandOrSearch(Esearch, index, variable1, variable2, fuz, size):
   a = Esearch.search(index=index,doc_type=dt1, body={
        "query": {
            "bool": {
                "must": [
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable1,
                            "fuzziness": fuz,
                            "operator": "or"
                        }
                    },
                    # AND
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable2,
                            "fuzziness": fuz,
                            "operator": "or"
                        }
                    }

                ]
            }
        }
       , "size": size
    })

   return a


def BoostSearch(Esearch, index, variable1, Boost, fuz, size):
   b = Esearch.search(index=index, doc_type=dt1,body={
        "query": {
            "bool": {
                "must": {
                    "multi_match": {
                        "fields": ["_all"],
                        "query": variable1,
                        "fuzziness": fuz,
                        "operator": "or"
                    }
                    #AND / OR
                    # {}
                },
                "should": {
                    "multi_match": {
                        "fields": ["_all"],
                        "query": Boost,
                        "fuzziness": "AUTO",
                        "boost": 4
                        }
                    }
            }

        }
       , "size": size

   })
   return b


def testFunction(expected, actual):
    if expected == actual:
        return True
    else:
        return False

def getResultsString(expected, actual):
    text = ""
    text += "Expected: " + str(expected) + "\n"
    text += "Actual: " + str(actual) + "\n"
    # text += str(testFunction(expected,actual)) + "\n"
    return text



########################################################################################################################

def calcPrecision(expected, actual):
    #Calculates precision : TF/
    if not expected:
        return int(actual == expected)
    else:
        if not actual:
            return 0
        else:
            ex = set(expected)
            act = set(actual)
            truePos = len(ex & act)
            return truePos / len(act)


def calcRecall(expected, actual):
    #Calculates recall:
    ex = set(expected)
    act = set(actual)
    truePos = len(ex & act)
    try:
        return truePos / float(len(ex))
    except:
        return 1


########################################################################################################################



def test1 (size):

    expectedGPC = [106, 386, 411, 412, 282]
    expectedMK = [99, 332,345]
    expectedGPCMK = []

    var = "keyboard mouse"
    var2 = "Gaming PC"
    boost = ""
    boost2 = "Wireless"
    fuzzy = 0
    sloppy = 0

    text = ""

    andSearch2 = ANDSearch(es,id,var2,fuzzy, sloppy, size)
    orSearch2 = ORSearch(es,id,var2,fuzzy, sloppy, size)

    andSearch = ANDSearch(es,id,var,fuzzy, sloppy, size)
    orSearch = ORSearch(es,id,var,fuzzy, sloppy, size)
    andORSearch = andORandSearch(es,id,var, var2,fuzzy, size)
    orANDSearch = OrandOrSearch(es,id,var, var2,fuzzy, size)
    boostSearch = BoostSearch(es,id,var, boost,fuzzy, size)
    boost2Search = BoostSearch(es, id, var, boost2,fuzzy, size)

    text += "\n"
    text += "AND: " + var+"\n"
    text += "Expected: " + str(len(expectedMK)) + " results\n"

    AND = Output(andSearch)
    expectedAND = expectedMK

    text += getResultsString(expectedAND,AND)

    precision = calcPrecision(expectedMK, AND)
    recall = calcRecall(expectedMK, AND)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"

    text += "\n***********************************************************************************\n"
    text += "OR: " + var+"\n"
    text += "Expected: " + str(len(expectedMK)) + " results\n"
    OR=Output(orSearch)
    expectedOR = expectedMK
    text += getResultsString(expectedOR,OR)

    precision = calcPrecision(expectedMK, OR)
    recall = calcRecall(expectedMK, OR)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"




    text += "\n***********************************************************************************\n"

    text += "\n"
    text += "AND: " + var2+"\n"
    text += "Expected: " + str(len(expectedGPC)) + " results\n"

    AND = Output(andSearch2)
    expectedAND = expectedGPC

    text += getResultsString(expectedAND,AND)

    precision = calcPrecision(expectedGPC, AND)
    recall = calcRecall(expectedGPC, AND)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"

    text += "\n***********************************************************************************\n"
    text += "OR: " + var2+"\n"
    text += "Expected: " + str(len(expectedGPC)) + " results\n"
    OR=Output(orSearch2)
    expectedOR = expectedGPC
    text += getResultsString(expectedOR,OR)

    precision = calcPrecision(expectedGPC, OR)
    recall = calcRecall(expectedGPC, OR)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"




    text += "\n***********************************************************************************\n"
    text += "AND: " + var + " : OR : " + "AND: " + var2+"\n"
    text += "Expected: " + str(len(expectedGPCMK)) + " results\n"
    andOR = Output(andORSearch)
    text += getResultsString(expectedGPCMK,andOR)

    precision = calcPrecision(expectedGPCMK, andOR)
    recall = calcRecall(expectedGPCMK, andOR)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"


    text += "\n***********************************************************************************\n"
    text += "OR: " + var + " : AND : " + "OR : " + var2+"\n"
    text += "Expected: " + str(len(expectedGPCMK)) + " results\n"
    orAND = Output(orANDSearch)

    text += getResultsString(expectedGPCMK,orAND)

    precision = calcPrecision(expectedGPCMK, orAND)
    recall = calcRecall(expectedGPCMK, orAND)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"


    text += "\n***********************************************************************************\n"
    text += "OR: " + var + " : BOOST : " + boost+"\n"
    text += "Expected: " + str(len(expectedMK)) + " results\n"
    Boost = Output(boostSearch)

    # expectedBoost.append(["11 Anakin Skywalker"])
    text += getResultsString(expectedMK,Boost)

    precision = calcPrecision(expectedMK, Boost)
    recall = calcRecall(expectedMK, Boost)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"


    text += "\n***********************************************************************************\n"
    text += "OR: " + var + " : BOOST : " + boost2+"\n"
    text += "Expected: " + str(len(expectedMK)) + " results\n"
    Boosted = Output(boost2Search)

    text += getResultsString(expectedMK,Boosted)

    precision = calcPrecision(expectedMK, Boosted)
    recall = calcRecall(expectedMK, Boosted)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"

    return text

def fuzzyTest(fuzzy, var, expectedWK, size):
    text =""
    fuzzy = fuzzy
    slop = 0
    var = var


    fuza = ANDSearch(es, id, var, fuzzy, slop, size)
    fuzb = ORSearch(es, id, var, fuzzy, slop, size)

    text += "\n\nAND: " + var+"\n"
    text += "Expected: " + str(len(expectedWK)) + " results\n"

    fuzAND = Output(fuza)
    text += getResultsString(expectedWK,fuzAND)

    precision = calcPrecision(expectedWK, fuzAND)
    recall = calcRecall(expectedWK, fuzAND)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"

    text += "\n***********************************************************************************\n"


    text += "\n\nOR: " + var+"\n"
    text += "Expected: " + str(len(expectedWK)) + " results\n"
    fuzOR = Output(fuzb)

    text += getResultsString(expectedWK,fuzOR)

    precision = calcPrecision(expectedWK, fuzOR)
    recall = calcRecall(expectedWK, fuzOR)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"


    text += "\n***********************************************************************************\n"
    return text


def test2 (var, size, expect):

    varry = var
    # varry = "wirelesh keybored mouse"
    expected = expect

    text = ""

    text += "\nBaseLine Fuzzy : 0\n\n"
    fuzzy = 0
    text += fuzzyTest(fuzzy, varry, expected, size)

    text += "\n\nFuzzy : AUTO\n"
    fuzzy = "AUTO"
    text += fuzzyTest(fuzzy, varry, expected, size)

    text += "\n\nFuzzy : 1\n"
    fuzzy = 1
    text += fuzzyTest(fuzzy, varry, expected, size)

    text += "\n\nFuzzy : 3\n"
    fuzzy = 3
    text +=fuzzyTest(fuzzy, varry, expected, size)


    text += "\n\nFuzzy : 6\n"
    fuzzy = 6
    text +=fuzzyTest(fuzzy, varry, expected, size)


    text += "\n\nFuzzy : 10\n"
    fuzzy = 10
    text +=fuzzyTest(fuzzy, varry, expected, size)
    return text

def slopTest (slopp, var, size, expect):

    text = ""
    fuzzy = "AUTO"
    slop = slopp
    var = var

    expectedSlop = expect

    slopa = ANDSearch(es, id, var, fuzzy,slop, size)
    slopb = ORSearch(es, id, var, fuzzy,slop, size)

    text += "\n\nAND: " + var+"\n"
    text += "Expected: " + str(len(expectedSlop)) + " results\n"
    slopAND = Output(slopa)
    text += getResultsString(expectedSlop,slopAND)

    precision = calcPrecision(expectedSlop, slopAND)
    recall = calcRecall(expectedSlop, slopAND)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"


    text += "\n***********************************************************************************\n"
    text += "\nOR: " + var+"\n"
    text += "Expected: " + str(len(expectedSlop)) + " results\n"
    slopOR = Output(slopb)
    text += getResultsString(expectedSlop,slopOR)

    precision = calcPrecision(expectedSlop, slopOR)
    recall = calcRecall(expectedSlop, slopOR)
    text += "Precision: " + str(precision) + "\n"
    text += "Recall: " + str(recall) + "\n"

    text += "\n***********************************************************************************\n"
    return text


def test3(var, size, expect):
    text = ""

    expected = expect

    varry = var


    text += "\n\nBaseLine Slop : 0\n"
    slopy = 0
    text += slopTest(slopy, varry, size, expected)

    text += "\n\nSlop : 1\n"
    slopy = 1
    text += slopTest(slopy, varry, size, expected)

    text += "\n\nSlop : 5\n"
    slopy = 5
    text += slopTest(slopy, varry, size, expected)

    text += "\n\nSlop : 10\n"
    slopy = 10
    text += slopTest(slopy, varry, size, expected)


    text += "\n\nSlop : 25\n"
    slopy = 25
    text += slopTest(slopy, varry, size, expected)
    return text

########################################################################################################################
########################################################################################################################
########################################################################################################################

def runTests():
    expectedGPC = [106, 386, 411, 412, 282]
    expectedMK = [99, 332,345]
    text = ""
    
    
    text+="\n"+("------------------------------------------------------------------------------------------------------------------------------------")
    text+="\n"+("Running Tests with size 10......***************************************************************************************************")
    text+="\n"+("------------------------------------------------------------------------------------------------------------------------------------\n")
    
    
    text+="\n"+("test1: \n"+"ing test 1......**************************************************************************************************************")
    text+="\n"+(test1(10))
    text+="\n"+("\n\n End test 1.")
    
    
    FuzPhrase = "krybaord mosue"
    text+="\n"+("\n\n\n Running test 2: "+FuzPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test2(FuzPhrase,10, expectedMK))
    text+="\n"+("\n\n End test 2.")
    
    
    FuzPhrase = "gamnif pe"
    text+="\n"+("\n\n\n Running test 2: "+FuzPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test2(FuzPhrase,10, expectedGPC))
    text+="\n"+("\n\n End test 2.")
    
    
    SlopPhrase = "mouse keyboard"
    text+="\n"+("\n\n\n Running test 3 : "+SlopPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test3(SlopPhrase,10, expectedMK))
    text+="\n"+("\n\n End test 3.")
    
    SlopPhrase = "pc gaming"
    text+="\n"+("\n\n\n Running test 3 : "+SlopPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test3(SlopPhrase,10, expectedGPC))
    text+="\n"+("\n\n End test 3.\n\n")
    
    
    
    
    
    
    text+="\n"+("------------------------------------------------------------------------------------------------------------------------------------")
    text+="\n"+("Running Tests with size 15......***************************************************************************************************")
    text+="\n"+("------------------------------------------------------------------------------------------------------------------------------------\n")
    
    text+="\n"+("\n"+"ing test 1......**************************************************************************************************************")
    text+="\n"+(test1(15))
    text+="\n"+("\n\n End test 1.")
    
    
    FuzPhrase = "krybaord mosue"
    text+="\n"+("\n\n\n Running test 2: "+FuzPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test2(FuzPhrase,15, expectedMK))
    text+="\n"+("\n\n End test 2.")
    
    FuzPhrase = "gamnif pe"
    text+="\n"+("\n\n\n Running test 2: "+FuzPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test2(FuzPhrase,15, expectedGPC))
    text+="\n"+("\n\n End test 2.")
    
    
    SlopPhrase = "mouse keyboard"
    text+="\n"+("\n\n\n Running test 3 : "+SlopPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test3(SlopPhrase,15, expectedMK))
    text+="\n"+("\n\n End test 3.")
    
    SlopPhrase = "pc gaming"
    text+="\n"+("\n\n\n Running test 3: "+SlopPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test3(SlopPhrase,15, expectedGPC))
    text+="\n"+("\n\n End test 3.\n\n")
    
    
    
    text+="\n"+("------------------------------------------------------------------------------------------------------------------------------------")
    text+="\n"+("Running Tests with size 30......***************************************************************************************************")
    text+="\n"+("------------------------------------------------------------------------------------------------------------------------------------\n")
    
    text+="\n"+("\n"+"ing test 1......**************************************************************************************************************")
    text+="\n"+(test1(30))
    text+="\n"+("\n\n End test 1.")
    
    
    FuzPhrase = "krybaord mosue"
    text+="\n"+("\n\n\n Running test 2: "+FuzPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test2(FuzPhrase,30, expectedMK))
    text+="\n"+("\n\n End test 2.")
    
    FuzPhrase = "gamnif pe"
    text+="\n"+("\n\n\n Running test 2: "+FuzPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test2(FuzPhrase,30, expectedGPC))
    text+="\n"+("\n\n End test 2.")
    
    SlopPhrase = "mouse keyboard"
    text+="\n"+("\n\n\n Running test 3 : "+SlopPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test3(SlopPhrase,30, expectedMK))
    text+="\n"+("\n\n End test 3.")
    
    SlopPhrase = "pc gaming"
    text+="\n"+("\n\n\n Running test 3: "+SlopPhrase+" .....*********************************************************************************************************")
    text+="\n"+(test3(SlopPhrase,30, expectedGPC))
    text+="\n"+("\n\n End test 3.\n\n")

    return text

########################################################################################################################
########################################################################################################################
########################################################################################################################








### Dont think this is needed####


# def testSearchEngine():
#     expectedGPC = [106, 386, 411, 412, 282]
#     expectedWK = [341, 342, 344, 345, 258]
#
#     var = "wireless keyboard mouse"
#     var2 = "Gaming PC intel"
#     boost = ""
#     boost2 = ""
#     fuzzy = 0
#     sloppy = 0
#
#     text = ""
#
#
#     andSearch = ANDSearch(es, id, var, fuzzy, sloppy)
#     orSearch = ORSearch(es, id, var, fuzzy, sloppy)
#
#     text += "\n"
#     text += "AND: " + var + "\n"
#     text += "Expected: "+str(len(expectedWK)) + " results\n"
#     AND = Output(andSearch)
#     text += getResultsString(expectedWK, AND)
#     precision = calcPrecision(expectedWK, AND)
#     recall = calcRecall(expectedWK, AND)
#     text += "Precision: " + str(precision) + "\n"
#     text += "Recall: " + str(recall) + "\n"
#
#     text += "\n***********************************************************************************\n"
#     text += "OR: " + var + "\n"
#     text += "Expected: " + str(len(expectedWK)) + " results\n"
#     OR = Output(orSearch)
#     text += getResultsString(expectedWK, OR)
#     precision = calcPrecision(expectedWK, OR)
#     recall = calcRecall(expectedWK, OR)
#     text += "Precision: " + str(precision) + "\n"
#     text += "Recall: " + str(recall) + "\n"
#
#     text += "\n***********************************************************************************\n"
#     text += "\n-----------------------------------------------------------------------------------\n"
#     andSearch = ANDSearch(es, id, var2, fuzzy, sloppy)
#     orSearch = ORSearch(es, id, var2, fuzzy, sloppy)
#
#     text += "\n"
#     text += "AND: " + var2 + "\n"
#     text += "Expected: "+str(len(expectedGPC)) + " results\n"
#     AND = Output(andSearch)
#     text += getResultsString(expectedGPC, AND)
#     precision = calcPrecision(expectedGPC, AND)
#     recall = calcRecall(expectedGPC, AND)
#     text += "Precision: " + str(precision) + "\n"
#     text += "Recall: " + str(recall) + "\n"
#
#     text += "\n***********************************************************************************\n"
#     text += "OR: " + var2 + "\n"
#     text += "Expected: " + str(len(expectedGPC)) + " results\n"
#     OR = Output(orSearch)
#     text += getResultsString(expectedGPC, OR)
#     precision = calcPrecision(expectedGPC, OR)
#     recall = calcRecall(expectedGPC, OR)
#     text += "Precision: " + str(precision) + "\n"
#     text += "Recall: " + str(recall) + "\n"
#
#
#     return text
#
#
# text+="\n"+("\n\n\n Running test 4.....*****************************************************************************************************")
# text+="\n"+(testSearchEngine())


