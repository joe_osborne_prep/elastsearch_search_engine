import re
import MainEngine

input = ""

def main(input, collections):
    result = ""

    if check(input):
        input = re.findall('[^()]+', input)

        firstQ = input[0]
        op = input[1]
        secQ = input[2]

        result += check1(firstQ, op, secQ, collections)
        result += check2(firstQ, op, secQ, collections)

    else:
        result += MainEngine.query(input, collections)

    return result


def check(input):
    input = re.findall('[^()]+', input)
    if len(input) > 1:
        return True
    else:
        return False

def check1(firstQ, op, secQ, collections):
    if ("&" in firstQ and "|" in op and "&" in secQ):
        print("Apply AND OR AND Search")

        var1 = firstQ.replace("&", "")
        var2 = secQ.replace("&", "")

        return MainEngine.queryANDOR(var1, var2, collections)
    else:
        return ""


def check2(firstQ, op, secQ, collections):
    if ("|" in firstQ and "&" in op and "|" in secQ):
        print("Apply OR AND OR Search")

        var1 = firstQ.replace("|", "")
        var2 = secQ.replace("|", "")

        return MainEngine.queryORAND(var1, var2, collections)
    else:
        return ""



