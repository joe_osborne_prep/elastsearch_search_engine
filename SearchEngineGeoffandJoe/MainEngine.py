import requests
import json
from elasticsearch import Elasticsearch
import Querier
from Scraper import COLLECTIONS_DIR
import os

#####Functions#######


def readFile(filename):
    #reads in a json file and returns a dictionary containing the items in the file
    d = {}

    with open(os.path.join(COLLECTIONS_DIR,filename)) as json_data:
        d = json.load(json_data)
        json_data.close()
    return d

def indexJson(filename, es):
    #First checks if the file has already been indexed
    #If it has not then it will be indexed using a fixed structure, universal for
    #all json files the Scraper class writes
    if not elas.indices.exists(filename):
        dataDict = readFile(filename+'.json')

        for data in dataDict:
            print(data)
            es.index(index=filename ,doc_type='Pages',id =dataDict[data]['ID'], body=dataDict[data])

def queryData(dataSet,vari, es):
    #Returns a dictionary containing the results of a query submitted to elastic search with variable
    #taken in the arguments
    e = es.search(index=dataSet,
                  body={"query": {"multi_match": {"fields": ["_all"], "query": vari, "fuzziness": "AUTO"}}})
    return e

def getResultsScrapedText(dataset, queryDict):
    #formats the results generated from a query in an appropriate text format
    output = "Data Set: " + dataset+  "\n\n"
    for data in queryDict['hits']['hits']:
        output += "Document ID: " + data['_id'] + "\n"
        output += "Score: " + str(data['_score']) + "\n"
        output += "URL: " + data['_source']['URL'] + "\n"
        output += "Title: " + data['_source']['Title'].lstrip().rstrip() + "\n"
        output += "Content: " + data['_source']['Content'][:100] + "...\n\n"
    return output

def getResultsStarWars(dataset, queryDict):
    # formats the results generated from a query in an appropriate text format
    output ="Data Set: " + dataset + "\n\n"
    for data in queryDict['hits']['hits']:
        output += "Document ID: " + data['_id'] + "\n"
        output += "Score: " + str(data['_score']) + "\n"
        output += "Name: "
        try:
            output += data['_source']['name']
        except:
            output += data['_source']['title']
        output += "\n\n"

    return output

def indexStarWarsCollection(req, docId, es):
    #First checks if the collection has already been indexed
    #If it has not then it will be indexed using a fixed structure, universal for
    #all json files the Scraper class writes
    if not elas.indices.exists(docId):
        i = 1
        print("called")
        while req.status_code == 200:
            req = requests.get('http://swapi.co/api/people/' + str(i))
            es.index(index=docId, doc_type="people", id=i, body=(req.content))
            i = i + 1
            if (i == 17 or i == 89):
                print("character bio's not available")
                i +=1

def main(collections):
    #The main function called which loops through all collection file names and attempts
    #to index them
    for collection in collections:
        print(collection)

        if "starwars" == collection:
            indexStarWarsCollection(request, collection, elas)
        else:
            indexJson(collection,elas)

def query(var, collections):

    text = ""
    for collection in collections:
        queryDict = queryData(collection, var, elas)
        text += getQueryText(collection,queryDict)
    return text

def getQueryText(col, queryRes):
    #simply manages what to return based on the collection
    text = ""
    if col == "starwars":
        text += getResultsStarWars(col, queryRes)
    else:
        text += getResultsScrapedText(col, queryRes)
    text += "\n"
    return text

def queryAND(var, collections):
    #Calls the AND function from the querier class, formats the text and returns
    text = ""
    for collect in collections:
        queryRes = Querier.ANDSearch(elas, collect, var)
        text += getQueryText(collect,queryRes)
    return text

def queryOR(var, collections):
    #Calls the OR function from the querier class, formats the text and returns
    text = ""
    for collect in collections:
        queryRes = Querier.ORSearch(elas, collect, var)
        text += getQueryText(collect,queryRes)
    return text

def queryANDOR(var1, var2, collections):
    #Calls the AND OR function from the querier class, formats the text and returns
    text = ""
    for collection in collections:
        queryRes = Querier.OrandOrSearch(elas, collection, var1, var2)
        text += getQueryText(collection, queryRes)
    return text

def queryORAND(var1, var2,collections):
    #Calls the OR AND function from the querier class, formats the text and returns
    text = ""
    for collection in collections:
        queryRes = Querier.andORandSearch(elas, collection, var1, var2)
        text += getQueryText(collection, queryRes)
    return text

def queryBoost(var1,boost, collections):
    #Calls the Boosting function from the querier class, formats the text and returns
    text = ""
    for collection in collections:
        queryRes = Querier.boostSearch(elas, collection, var1, boost)
        text += getQueryText(collection, queryRes)
    return text

request = requests.get('http://localhost:9200')
elas = Elasticsearch([{'host': 'localhost', 'port': 9200}])
