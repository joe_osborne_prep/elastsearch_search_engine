# README #

### What is this repository for? ###
This script will scrape webpages for their content and will index them using a local Elasticsearch server to hold the information. This data can then be queried. 
This was a joint effort from myself (Joe Osborne) and a friend and colleague (Geoff Henderson).


### How do I get set up? ###
The script will run as it is but will require a running Elastic search server that will need to be downloaded and executed before running the script.
SearchEngineLauncher.py is what needs to be run in order to execute.


### Contribution guidelines ###
Some tests have been included and can be accsessed through the GUI.

* Code review


### Who do I talk to? ###

Repo owner or admin
