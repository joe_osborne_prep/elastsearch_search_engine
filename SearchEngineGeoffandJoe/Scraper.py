import scrapy
from bs4 import BeautifulSoup
import re
import requests
from scrapy.crawler import CrawlerProcess
import os
import json

COLLECTIONS_DIR = "Collections"
if not os.path.exists(COLLECTIONS_DIR):
    os.makedirs(COLLECTIONS_DIR)

data = {}

def extractMeta(url, metaTag):
    html = requests.get(url).text
    soup = BeautifulSoup(html, "html.parser")
    keyW = ""
    for meta in soup.findAll("meta"):
        metaname = meta.get('name', '').lower()
        metaprop = meta.get('property', '').lower()
        if metaTag == metaname or metaprop.find(metaTag) > 0:
            keyW = meta['content'].strip()
    return keyW.strip(",")

class WebItem(scrapy.Item):
    link = scrapy.Field()
    content = scrapy.Field()

class WebSpider(scrapy.Spider):
    name = "link extractor"
    docID = 0

    def __init__(self, urlArg , **kwargs):
        super().__init__(**kwargs)
        self.allowed_domains = []
        self.start_urls = [urlArg]
        self.BASE_URL = urlArg

    def parse(self, response):
        hxs = scrapy.Selector(response)
        links = hxs.select('//a[contains(@href, "")]/@href').extract()
        for link in links:
            if (link.startswith("/")):
                absolute_url = self.BASE_URL + link
                yield scrapy.Request(absolute_url, callback=self.parse_attr)

    def getSubHeadings(self, heading, soup):
        subheadings = soup.find_all(heading)
        subheadingString = ""
        for h3 in subheadings:
            subheadingString += h3.text + " "
        return subheadingString

    def getWholeContent(self,soup):
        for script in soup(["script", "style"]):
            script.extract()
        text = soup.get_text()

        text = re.sub(r'[\t\r\n]','', text)
        return text

    def parse_attr(self, response):
        #
        item = WebItem()
        item['link'] = response.url
        soup = BeautifulSoup(response.text)
        meta = extractMeta(item['link'],"description")
        subheadingString = self.getSubHeadings("h3",soup)
        #body = self.getSubHeadings("p",soup)

        data[self.docID]= ({
            'ID': self.docID,
            'URL': item['link'],
            'Title': soup.title.string,
            'Content': meta,
            'Listings': subheadingString
            #'Body' : body
        })

        self.docID+=1
        return item

def scrapeWebsite( url):
    #Launches the web, passing in a url to be scraped
    #Extracts the filename by seperating the url.
    #Writes the result of scrapy to a json file
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })
    process.crawl(WebSpider, urlArg=url)
    process.start()

    filename = os.path.join(COLLECTIONS_DIR, url.split(".")[1] + '.json')

    with open(filename, 'w') as json_data:
        json.dump(data, json_data, indent=1)
        json_data.close()