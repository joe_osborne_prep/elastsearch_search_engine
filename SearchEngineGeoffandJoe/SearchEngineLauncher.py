from tkinter import *
import MainEngine
import Scraper
import SplitCheck
import QueryRunner
import os

MAX_COLUMNS = 5

class UserInterface:
    def __init__(self, master):
        self.master = master
        self.collectionsFrame = Frame(self.master)
        self.collectionsCheckBoxes = dict()
        self.collections = []
        master.title("Joff Search Engine")

        Label(master, text="Welcome!").pack()

        self.scrapeURL = StringVar()
        self.scrapeURLEntry()

        Label(master, text="Choose Collection: ").pack()

        self.addAllCollectionsCheckboxes()

        self.indexButton = Button(master, text="Index", command=self.indexCollection)
        self.indexButton.pack()

        Label(master, text="Enter keywords:").pack()

        self.keywordText = Entry(master, text="http://www.essex.ac.uk")
        self.keywordText.pack()

        Label(master, text="Enter BoostWord:").pack()

        self.boostText = StringVar()
        Entry(master, text="boost", textvariable=self.boostText).pack()

        Button(master, text="Go!", command=self.getResults).pack()
        Button(master, text="AND", command=self.doAND).pack()
        Button(master, text="BOOST", command=self.boost).pack()
        Button(master, text="Run Evaluation", command=self.runEvaluation).pack()

        self.text = Text(height=50, width=90)
        self.scroll = Scrollbar(root)
        self.text.configure(yscrollcommand=self.scroll.set)
        self.text.pack()

    def scrapeURLEntry(self):
        def getUrl():
            if self.scrapeURL.get():
                Scraper.scrapeWebsite(self.scrapeURL.get())
                submitButton["state"] = DISABLED
                self.refreshCollectionsCheckboxes()

        scrapeFrame = Frame(self.master)
        Entry(scrapeFrame, textvariable=self.scrapeURL).pack(side=LEFT)
        submitButton = Button(scrapeFrame, text="Scrape", command=getUrl)
        submitButton.pack(side=RIGHT)
        scrapeFrame.pack()

    def addAllCollectionsCheckboxes(self):
        counter = 0
        for filename in os.listdir("Collections"):
            if counter == 0:
                self.addCollection("starwars", counter)
                counter += 1
            self.addCollection(filename.replace(".json",""), counter)
            counter += 1

        if (counter == 0):
            self.addCollection("starwars", counter)
        self.collectionsFrame.pack()

    def refreshCollectionsCheckboxes(self):
        self.collectionsCheckBoxes.clear()
        self.collections.clear()

        for child in self.collectionsFrame.winfo_children():
            child.destroy()

        self.addAllCollectionsCheckboxes()

    def addCollection(self, name, counter):
        collectionFrame = Frame(self.collectionsFrame)
        self.collectionsCheckBoxes[name] = IntVar()
        Checkbutton(collectionFrame, variable=self.collectionsCheckBoxes[name]).pack(side=LEFT)
        Label(collectionFrame, text=name).pack(side=RIGHT)
        collectionFrame.grid(row=counter // MAX_COLUMNS, column=counter % MAX_COLUMNS)

    def writeTextToBox(self, text):
        self.text.delete('1.0', END)
        self.text.insert(END, text)

    def getResults(self):
        text = self.keywordText.get()
        text = SplitCheck.main(text, self.collections)
        self.writeTextToBox(text)

    def indexCollection(self):
        text = "Indexing...\n"
        self.writeTextToBox(text)
        self.collections = [k for k, v in self.collectionsCheckBoxes.items() if v.get() == 1]
        MainEngine.main(self.collections)
        self.writeTextToBox("Finished!")

    def doAND(self):
        text = MainEngine.queryAND(self.keywordText.get(), self.collections)
        self.writeTextToBox(text)

    def boost(self):
        query = self.keywordText.get()
        boostTerm = self.boostText.get()

        print(query)
        print(boostTerm)

        text = MainEngine.queryBoost(query, boostTerm, self.collections)
        self.writeTextToBox(text)

    def runEvaluation(self):
        MainEngine.indexStarWarsCollection(MainEngine.request, "starwars", MainEngine.elas)
        text = QueryRunner.runTests()
        self.writeTextToBox(text)

root = Tk()
my_gui = UserInterface(root)
root.mainloop()
