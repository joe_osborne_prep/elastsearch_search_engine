def ANDSearch(Esearch, index, variable):

    e=Esearch.search(index=index, body={
        "query": {
            "multi_match": {
                "fields": ["_all"],
                "query": variable,
                "fuzziness": "AUTO",
                "operator": "and"
            }
        }

    })

    return e

def ORSearch(Esearch, index, variable):
    e=Esearch.search(index=index, body={
        "query": {
            "multi_match": {
                "fields": ["_all"],
                "query": variable,
                "fuzziness": "AUTO",
                "operator": "or"
            }
        }

    })

    return e

def andORandSearch(Esearch, index, variable1, variable2):
   z = Esearch.search(index=index, body={
        "query": {
            "bool": {
                "should": [
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable1,
                            "fuzziness": "AUTO",
                            "operator": "and"
                        }
                    },
                    # OR
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable2,
                            "fuzziness": "AUTO",
                            "operator": "and"
                        }
                    }

                ]
            }
        }
    })

   return z


def OrandOrSearch(Esearch, index, variable1, variable2):
   a = Esearch.search(index=index, body={
        "query": {
            "bool": {
                "must": [
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable1,
                            "fuzziness": "AUTO",
                            "operator": "or"
                        }
                    },
                    # AND
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable2,
                            "fuzziness": "AUTO",
                            "operator": "or"
                        }
                    }

                ]
            }
        }
    })

   return a

def OrandANDSearch(Esearch, index, variable1, variable2):
   a = Esearch.search(index=index, body={
        "query": {
            "bool": {
                "must": [
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable1,
                            "fuzziness": "AUTO",
                            "operator": "or"
                        }
                    },
                    # AND
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable2,
                            "fuzziness": "AUTO",
                            "operator": "and"
                        }
                    }

                ]
            }
        }
    })

   return a

def ORorANDSearch(Esearch, index, variable1, variable2):
   a = Esearch.search(index=index, body={
        "query": {
            "bool": {
                "should": [
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable1,
                            "fuzziness": "AUTO",
                            "operator": "or"
                        }
                    },
                    # AND
                    {
                        "multi_match": {
                            "fields": ["_all"],
                            "query": variable2,
                            "fuzziness": "AUTO",
                            "operator": "and"
                        }
                    }

                ]
            }
        }
    })

   return a

def boostSearch(Esearch, index, variable1, Boost):
   b = Esearch.search(index=index ,body={
        "query": {
            "bool": {
                "must": {
                    "multi_match": {
                        "fields": ["_all"],
                        "query": variable1,
                        "fuzziness": "AUTO",
                        "operator": "or"
                    }
                    #AND / OR
                    # {}
                },
                "should": {
                    "multi_match": {
                        "fields": ["_all"],
                        "query": Boost,
                        "fuzziness": "AUTO",
                        "boost": 3
                        }
                    }
            }

        }
   })

   return b


